#!/usr/bin/env python                                                                                               
'''Take a phrase and transform each character into the integer that 
represents its index in the alphabet, separated by commas'''
toggle=raw_input("Are you encrypting/decrypting? [1/2] \n")

if toggle=="1" :
    string=raw_input("Choose your sentence: \n")
    string=string.lower()
    
    for char in string :
        if char=="," : pass 
        if char.isalpha()==False and char!=",": print char,
        else: print ord(char)-97, ",",   
        
elif toggle=="2" :

    # Example message:
    # string="13 , 4 , 21 , 4 , 17 ,   6 , 8 , 21 , 4 ,   20 , 15 , !   13 , 4 , 21 , 4 , 17 ,   18 , 20 , 17 , 17 , 4 , 13 , 3 , 4 , 17 , !"
    string=raw_input("Enter message to decrypt: \n")
    
    myList=string.split(",")
    
    for char in myList :
        n=0
        while n<len(char) and char[n].isdigit()==False :
            print char[n],
            n+=1
        if n<len(char) :
            print chr(int(char[n:])+97),
        
else: print "Please enter a valid operation."
    
