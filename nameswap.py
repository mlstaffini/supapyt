#!/usr/bin/env python

''' 
Define a string with your name with your name and write a script (in a .py plain text file) to provide your name in reverse with all vowels capitalised and all consonants lower case.
'''

name = raw_input ( "What's your name? \n")

name= name[::-1]

def isVowel(x) :
    if x=="a" or x=="e" or x=="i" or x=="o" or x=="u" :
        return True

for char in name :
        if isVowel(char) : 
            print char.upper(),
        else:
            print char.lower(),
