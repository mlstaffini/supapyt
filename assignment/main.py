#!/usr/bin/env python

# by Maria Laura Staffini (mls54@st-andrews.ac.uk)
#

import csv
from DataManipulation import *


##########################
# BOOLS FOR FILE OPS
closest_furthest=True
effective_temp=True
orbeccen=True
discovery_year=True
cool_close_collected=True
methods=True

filename='planets.csv'
#filename='dummyplanets.csv'

##########################
# READING IN

myList=[]

with open(filename, 'rb') as csvfile:
    dialect = csv.Sniffer()
#    reader = csv.reader(csvfile, dialect)
    for row in csvfile:
        if '#' in row:
            continue
        elif 'rowid' in row:
            continue
        else:
            row=row.replace("\n","").split(",")
            for i,x in enumerate(row):
                if len(x)<1: x=row[i]=None
            myList.append(dataObject(row[0],row[1:]))    

print
print "################################"
print
            
##########################
# CLOSEST & FURTHEST
# The closest and the furthest exoplanet. I know for a fact there is only one closest (woo astro knowledge)

if closest_furthest:
    ascending_st_dist=sortedBy(myList,'st_dist',False)

    popAllNone(ascending_st_dist,'st_dist')

    ## QUESTION: if I wanted to use a reduce lambda structure to do sorting e.g. lambda x,y: x if x>y else y how would I go about setting it up? I got confused about how to pull out the relevant quantities
    
    print
    print "The closest exoplanet is {0}, {1} pc away, and astronomy seems to agree with my code which is nice (you can find out more info about {0} on row {2} of the data file).".format(ascending_st_dist[0].get_pl_name(), ascending_st_dist[0].get('st_dist'), ascending_st_dist[0].get_rowid())
    
    if ascending_st_dist[-2].get('st_dist')==ascending_st_dist[-1].get('st_dist'):
        print "There is more than one exoplanet {0} pc away from Earth (furthest), here is all of them:".format(ascending_st_dist[-1].get('st_dist')),
        n=1
        print ascending_st_dist[-1].get_pl_name(), ",",
        while ascending_st_dist[-1-n].get('st_dist')==ascending_st_dist[-1].get('st_dist'):
            print ascending_st_dist[-1-n].get_pl_name()
            n+=1
    else:
        print "The furthest exoplanet is {0}, {1} pc away (you can find out more info about {0} on row {2} of the data file).".format(ascending_st_dist[-1].get_pl_name(), ascending_st_dist[-1].get('st_dist'), ascending_st_dist[-1].get_rowid())

print
print
    
##########################
# EFFECTIVE TEMPERATURES
# 

if effective_temp:
 
    ascending_st_teff=sortedBy(myList,'st_teff',False)

    popAllNone(ascending_st_teff,'st_teff')

    print "The lowest and highest effective temperatures respectively belong to {0} with {1}K and {2} with {3}K".format(ascending_st_teff[0].get_pl_name(),ascending_st_teff[0].get('st_teff'),ascending_st_teff[-1].get_pl_name(),ascending_st_teff[-1].get('st_teff'))

    mean_eff_temp=mean(ascending_st_teff,'st_teff')
                 
    print "The mean of the effective temperatures is {0:6.1f}.".format(mean_eff_temp)

print
print
    
##########################
# ORBITAL ECCENTRICITY
#

if orbeccen:

    ascending_orbeccen=sortedBy(myList,'pl_orbeccen',False)

    popAllNone(ascending_orbeccen,'pl_orbeccen')

    mean_orbeccen=mean(ascending_orbeccen,'pl_orbeccen')
    median_orbeccen=median(ascending_orbeccen,'pl_orbeccen')
    stdev_orbeccen=stdev(ascending_orbeccen,'pl_orbeccen',mean_orbeccen)

    print "The mean of the exoplanets' orbital eccentricity is {0:4.2f}, the median is {1:4.2f} and the standard deviation is {2:6.4f}.".format(mean_orbeccen, median_orbeccen, stdev_orbeccen)

print
print    
##########################
# DISCOVERY YEARS
#

if discovery_year:

    discovery=[]
    for x in myList:
        discovery.append(int(x.get('pl_disc')))

    years={}
    for x in set(discovery):
        years[x]=discovery.count(x)

    print "The following is a tally of how many planets were discovered on each year:"
    for x in sorted(years.iterkeys()):
        print x, ':', years[x], " ",
    print
    
print
print
##########################
# CLOSE, COOL & COLLECTED
#

if cool_close_collected:

    # I realise a single loop would have sufficed for all three with ifs and appends but I wanted to practice lambdas (also given that I used count and sets above)
    
    cool=list(filter(lambda x: x.get('st_teff')<3000 and x.get('st_teff') is not None, myList)) 
    close=list(filter(lambda x: x.get('st_dist')<30 and x.get('st_dist') is not None, myList)) 
    collected=list(filter(lambda x: x.get('pl_orbeccen')<0.2 and x.get('pl_orbeccen') is not None, myList))

    allthree=list(filter(lambda x: x.get('st_dist')<30 and x.get('st_dist') is not None and  x.get('pl_orbeccen')<0.2 and x.get('pl_orbeccen') is not None, cool))
    
    # and they're so pretty

    
    print "These planets are cooler than 3000K:"
    for x in cool:
        print x.get_pl_name(), "  ",

    print
    print
    print "These planets are closer than 30pc:"
    for x in close:
        print x.get_pl_name(), "  ",

    print
    print
    print "These planets have eccentricity <0.2:"
    for x in collected:
        print x.get_pl_name(), "  ",

    print
    print
    if allthree:
        print "Planets which have all three characteristics:"
        for x in allthree:
            print x.get_pl_name(), "  ",
    else:
        print "Unfortunately, no planet has all three characteristics simultaneously."    

print
print
##########################
# METHODS
#

# I've had a PSU breakdown in the last few days so I ended up working pretty close to the deadline; the following
# is reading something wrong and I'm not sure why, I'll edit it later if I have time, otherwise oh well.

if methods:
    
    methods=[]
    for x in myList:
        methods.append(x.get_pl_discmethod())

    for x in set(methods):

        mean_orbper=0
        n_orbper=0
        mean_plmass=0
        n_plmass=0
        mean_stmass=0
        n_stmass=0
        for y in myList:
            if y.get_pl_discmethod() is x and y.get('pl_orbper') is not None:
                mean_orbper+=mean_orbper+y.get('pl_orbper')
                n_orbper+=1
            if y.get_pl_discmethod() is x and y.get('pl_masse') is not None:
                mean_plmass+=mean_plmass+y.get('pl_masse')
                n_plmass+=1
            if y.get_pl_discmethod() is x and y.get('st_mass') is not None:
                mean_stmass+=mean_stmass+y.get('st_mass')
                n_stmass+=1
            if int(n_orbper)!=0:
                mean_orbper=mean_orbper/n_orbper
            if int(n_plmass)!=0:
                mean_plmass=mean_plmass/n_plmass
            if int(n_stmass)!=0:
                mean_stmass=mean_stmass/n_stmass
        print "For {0} mean orbital duration is {1}, mean planet mass is {2}, and mean stellar mass is {3}".format(x, mean_orbper, mean_plmass,mean_stmass)
            
        
print
print "################################"
print
print
