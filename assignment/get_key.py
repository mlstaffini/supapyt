#!/usr/bin/env python                                                                                           
import csv
import pickle

key={}

filename='planets.csv'                                                                                          
with open(filename, 'rb') as csvfile:
    dialect = csv.Sniffer()
    for row in csvfile:
        if '#' in row:
            continue
        elif 'rowid' in row:
            row=row.replace("\n","").split(",")
            for i,x in enumerate(row):
                key[x]=i-1
        else:
            break
            
print "Key of sorting methods:"
print key

with open('key_of_attributes.pkl','w') as f:
    pickle.dump(key,f)


#inp=raw_input("Which method do you need?")
#print key.get(inp, None)
